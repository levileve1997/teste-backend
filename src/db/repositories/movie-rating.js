import moment from 'moment-timezone';
import db from '../database';


export const MovieRatingEntity = db.models.MovieRating;

export default class MovieRatingRepository {
  static async create(movieRating, options) {
    let response = null;

    try {
      response = MovieRatingEntity.build(movieRating);

      response = await response.save({
        transaction: options ? options.transaction : null,
        returning: true,
      });
    } catch (err) {
      throw new Error('persistence', err);
    }

    return response;
  }

  static async selectOne(options) {
    let response = null;

    try {
      response = await MovieRatingEntity.findOne(options);
    } catch (err) {
      throw new Error('persistence', err);
    }

    return response;
  }

  static async count(options) {
    let response = null;

    try {
      response = await MovieRatingEntity.count(options);
    } catch (err) {
      throw new Error('persistence', err);
    }

    return response;
  }

  static async updateByUserIdAndMovieId(userId, movieId, movieRating, options) {
    let response = null;

    try {
      response = await MovieRatingEntity.update(movieRating, {
        where: { userId, movieId },
        transaction: options && options.transaction,
        returning: true,
      });

      [, [response]] = response;
    } catch (err) {
      throw new Error('persistence', err);
    }

    return response;
  }

  static async deleteByUserIdAndMovieId(userId, movieId, updateBy, options) {
    let response = null;

    try {
      response = await MovieRatingEntity.update({
        deletedAt: moment().toDate(),
        updateBy,
      }, {
        where: { userId, movieId },
        transaction: options && options.transaction,
        returning: true,
      });

      [, [response]] = response;
    } catch (err) {
      throw new Error('persistence', err);
    }

    return response;
  }
}
