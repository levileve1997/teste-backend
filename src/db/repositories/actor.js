import db from '../database';


export const ActorEntity = db.models.Actor;

export default class ActorRepository {
  static async bulkInsert(actors, options) {
    let response = null;

    try {
      response = await ActorEntity.bulkCreate(actors, {
        transaction: options ? options.transaction : null,
      });
    } catch (err) {
      throw new Error('persistence', err);
    }

    return response;
  }

  static async deleteByMovieId(movieId, options) {
    let response = null;

    try {
      response = await ActorEntity.destroy({
        where: { movieId },
        transaction: options && options.transaction,
      });

    } catch (err) {
      throw new Error('persistence', err);
    }

    return response;
  }
}
