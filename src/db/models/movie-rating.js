export default (sequelize, type) => {
  const MovieRating = sequelize.define('MovieRating',
    {
      id: {
        type: type.UUID, primaryKey: true, field: 'id', defaultValue: type.UUIDV4,
      },

      userId: { type: type.UUID, field: 'id_user' },
      movieId: { type: type.UUID, field: 'id_movie' },

      vote: { type: type.STRING, field: 'int_vote' },
      comment: { type: type.STRING, field: 'str_comment' },

      createBy: { type: type.STRING, field: 'str_create_by' },
      updateBy: { type: type.STRING, field: 'str_update_by' },
      createdAt: { type: type.DATE, field: 'dt_created_at' },
      updatedAt: { type: type.DATE, field: 'dt_updated_at' },
      deletedAt: { type: type.DATE, field: 'dt_deleted_at' },
    },
    {
      tableName: 'tb_movie_rating',
      freezeTableName: true,
      timestamps: true,

      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
      deletedAt: 'deletedAt',
    });

  MovieRating.associate = (models) => {

    MovieRating.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'userId',
    });

    MovieRating.belongsTo(models.Movie, {
      as: 'movie',
      foreignKey: 'movieId',
    });

  };

  return MovieRating;
};
