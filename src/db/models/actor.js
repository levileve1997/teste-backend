export default (sequelize, type) => {
  const Actor = sequelize.define('Actor',
    {
      id: {
        type: type.UUID, primaryKey: true, field: 'id', defaultValue: type.UUIDV4,
      },

      movieId: { type: type.UUID, field: 'id_movie' },

      name: { type: type.STRING, field: 'str_name' },

      createdAt: { type: type.DATE, field: 'dt_created_at' },
      updatedAt: { type: type.DATE, field: 'dt_updated_at' },
      deletedAt: { type: type.DATE, field: 'dt_deleted_at' },
    },
    {
      tableName: 'tb_actor',
      freezeTableName: true,
      timestamps: true,

      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
      deletedAt: 'deletedAt',
    });

  Actor.associate = (models) => {

    Actor.belongsTo(models.Movie, {
      as: 'movie',
      foreignKey: 'movieId',
    });

  };

  return Actor;
};
