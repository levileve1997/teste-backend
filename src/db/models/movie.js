export default (sequelize, type) => {
  const Movie = sequelize.define('Movie',
    {
      id: {
        type: type.UUID, primaryKey: true, field: 'id', defaultValue: type.UUIDV4,
      },

      director: { type: type.STRING, field: 'str_director' },
      name: { type: type.STRING, field: 'str_name' },
      genre: { type: type.INTEGER, field: 'int_genre' },

      createBy: { type: type.STRING, field: 'str_create_by' },
      updateBy: { type: type.STRING, field: 'str_update_by' },
      createdAt: { type: type.DATE, field: 'dt_created_at' },
      updatedAt: { type: type.DATE, field: 'dt_updated_at' },
      deletedAt: { type: type.DATE, field: 'dt_deleted_at' },
    },
    {
      tableName: 'tb_movie',
      freezeTableName: true,
      timestamps: true,

      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
      deletedAt: 'deletedAt',
    });

  Movie.associate = (models) => {

    Movie.hasMany(models.MovieRating, {
      as: 'movieRating',
      foreignKey: 'movieId',
    });

    Movie.hasMany(models.Actor, {
      as: 'actors',
      foreignKey: 'movieId',
    });

  };

  return Movie;
};
