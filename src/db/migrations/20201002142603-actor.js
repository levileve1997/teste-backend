export default {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_actor', {
      id: { type: Sequelize.UUID, primaryKey: true },

      id_movie: {
        type: Sequelize.UUID,
        references: {
          model: 'tb_movie',
          key: 'id',
        },
      },

      str_name: { type: Sequelize.STRING },

      dt_created_at: { type: Sequelize.DATE },
      dt_updated_at: { type: Sequelize.DATE },
      dt_deleted_at: { type: Sequelize.DATE },
    });

    await queryInterface.addIndex('tb_actor', ['id_movie']);
  },

  down: async (queryInterface) => {
    await queryInterface.removeIndex('tb_actor', ['id_movie']);

    await queryInterface.dropTable('tb_actor');
  },
};
