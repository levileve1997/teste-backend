export default {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_movie_rating', {
      id: { type: Sequelize.UUID, primaryKey: true },

      id_user: {
        type: Sequelize.UUID,
        references: {
          model: 'tb_user',
          key: 'id',
        },
      },
      id_movie: {
        type: Sequelize.UUID,
        references: {
          model: 'tb_movie',
          key: 'id',
        },
      },

      int_vote: { type: Sequelize.INTEGER },
      str_comment: { type: Sequelize.STRING },

      str_create_by: { type: Sequelize.STRING },
      str_update_by: { type: Sequelize.STRING },
      dt_created_at: { type: Sequelize.DATE },
      dt_updated_at: { type: Sequelize.DATE },
      dt_deleted_at: { type: Sequelize.DATE },
    });

    await queryInterface.addIndex('tb_movie_rating', ['id_user', 'id_movie']);
  },

  down: async (queryInterface) => {
    await queryInterface.removeIndex('tb_movie_rating', ['id_user', 'id_movie']);

    await queryInterface.dropTable('tb_movie_rating');
  },
};
