import jwt from 'jsonwebtoken';
import Constants from '../utilities/constants';


export default class JwtService {
  static createToken(createToken) {
    return jwt.sign({ id: createToken }, `${Constants.token.secret}`);
  }

  static verifyToken(verifyToken) {
    return jwt.verify(verifyToken, `${Constants.token.secret}`, { expiresIn: Constants.token.expiration });
  }
}
