import { literal } from 'sequelize';
import MovieRepository from '../db/repositories/movie';
import MovieRatingRepository, { MovieRatingEntity } from '../db/repositories/movie-rating';
import ActorRepository, { ActorEntity } from '../db/repositories/actor';
import { UserEntity } from '../db/repositories/user';
import { serviceOrderHelper } from '../utilities/utils';
import BusinessError, { ValidationCodeError } from '../utilities/errors/business';
import { getAllFilter } from './filters/movie';


export default class MovieService {
  static async create(movie, actor) {
    let response = {};

    const movieToCreate = {
      director: movie.director,
      name: movie.name,
      genre: movie.genre,

      createBy: actor.id,
      updateBy: actor.id,
    };

    const movieCreated = await MovieRepository.create(movieToCreate);

    const actors = movie.actors.map(name => ({ name, movieId: movieCreated.id }));
    await ActorRepository.bulkInsert(actors);

    response = await MovieService.getById(movieCreated.id);

    return response;
  }

  static async updateById(id, movie, actor) {
    let response = {};

    await MovieService.getSimpleById(id);

    const movieToSave = {
      director: movie.director,
      name: movie.name,
      genre: movie.genre,

      updateBy: actor.id,
    };

    await MovieRepository.updateById(id, movieToSave);

    if (movie.actors) {

      await ActorRepository.deleteByMovieId(id);

      const actors = movie.actors.map(name => ({ name, movieId: id }));
      await ActorRepository.bulkInsert(actors);
    }

    response = await MovieService.getById(id);

    return response;
  }

  static async movieRatingByMovieId(movieId, movieRating, actor) {
    let response = {};

    await MovieService.getSimpleById(movieId);

    const movieRatingToSave = {
      movieId,
      userId: actor.id,
      vote: movieRating.vote,
      comment: movieRating.comment,

      createBy: actor.id,
      updateBy: actor.id,
    };

    response = await MovieRatingRepository.selectOne({ where: { movieId, userId: actor.id } });

    if (response) { throw new BusinessError(ValidationCodeError.ENTITY_ALREADY_EXISTS, 'movieRating'); }

    await MovieRatingRepository.create(movieRatingToSave);

    response = await MovieService.getById(movieId);

    return response;
  }

  static async getSimpleById(id) {
    const response = await MovieRepository.selectOne({ where: { id, deletedAt: null } });

    if (!response) { throw new BusinessError(ValidationCodeError.ENTITY_NOT_FOUND, 'movie'); }

    return response;
  }

  static async getById(id) {
    let response = await MovieRepository.selectOne({
      where: { id, deletedAt: null },
      include: [{
        model: MovieRatingEntity,
        as: 'movieRating',
        attributes: ['vote', 'comment', 'createdAt'],
        required: false,
        where: { deletedAt: null },
        include: [{
          model: UserEntity,
          as: 'user',
          attributes: ['name'],
          required: false,
        }],
      }, {
        model: ActorEntity,
        as: 'actors',
        required: false,
        attributes: ['name'],
      }],
    });

    let totalVote = await MovieRatingRepository.selectOne({
      where: { movieId: id, deletedAt: null },
      attributes: [
        [literal('ROUND(CAST(SUM("int_vote") AS NUMERIC), 2)'), 'totalVote'],
      ],
    });

    ({ totalVote } = totalVote.toJSON());

    const totalUser = await MovieRatingRepository.count({ where: { movieId: id, deletedAt: null } });

    if (!response) { throw new BusinessError(ValidationCodeError.ENTITY_NOT_FOUND, 'movie'); }

    response = { ...response.toJSON(), votes: totalVote / totalUser };

    return response;
  }

  static async getAllWithPagination(searchParameter) {
    const { whereMovie, whereActor } = getAllFilter(searchParameter);
    const actorRequired = Object.values(whereActor).length > 1;
    return MovieRepository.selectWithPagination({
      where: whereMovie,
      offset: searchParameter.offset,
      limit: searchParameter.limit,
      order: [serviceOrderHelper(searchParameter)],
      include: [{
        model: ActorEntity,
        as: 'actors',
        required: actorRequired,
        where: whereActor,
        attributes: ['name'],
      }],
    });
  }

  static async deleteById(id, actor) {
    await MovieService.getSimpleById(id);
    await MovieRepository.deleteById(id, actor.id);
  }
}
