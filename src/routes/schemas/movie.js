import { idValidation } from './index';
import MovieGenre from '../../enumerators/movie-genre';

export const movieCreate = {
  genre: {
    in: 'body',
    custom: {
      options: value => Object.values(MovieGenre).some(o => o === value),
    },
    errorMessage: 'invalid_genre',
  },
  name: {
    in: 'body',
    isString: true,
    errorMessage: 'invalid_name',
  },
  director: {
    in: 'body',
    optional: true,
    isString: true,
    errorMessage: 'invalid_director',
  },
  actors: {
    in: 'body',
    isArray: true,
    errorMessage: 'invalid_actors',
  },
};

export const movieUpdate = {
  id: {
    ...idValidation,
  },
  genre: {
    in: 'body',
    optional: true,
    custom: {
      options: value => Object.values(MovieGenre).some(o => o === value),
    },
    errorMessage: 'invalid_genre',
  },
  name: {
    in: 'body',
    isString: true,
    optional: true,
    errorMessage: 'invalid_name',
  },
  director: {
    in: 'body',
    optional: true,
    isString: true,
    errorMessage: 'invalid_director',
  },
  actors: {
    in: 'body',
    isArray: true,
    optional: true,
    errorMessage: 'invalid_actors',
  },
};

export const movieVote = {
  id: {
    ...idValidation,
  },
  comment: {
    in: 'body',
    optional: true,
    isString: true,
    errorMessage: 'invalid_comment',
  },
  vote: {
    in: 'body',
    custom: {
      options: value => value <= 4 && value >= 0,
    },
    errorMessage: 'invalid_vote',
  },
};
