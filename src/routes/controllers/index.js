import express from 'express';

const router = express.Router();

router.use('/user', require('./user').default);
router.use('/movie', require('./movie').default);

export default router;
