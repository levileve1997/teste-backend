import ExtendableError from './extendable-error';

export default class BusinessError extends ExtendableError {
  constructor(code, options) {
    super();
    this.code = code;
    this.options = options;
  }
}

export const ValidationCodeError = {
  INVALID_ID: 'invalid_id',
  INVALID_PARAMS: 'invalid_params',
  ENTITY_NOT_FOUND: 'entity_not_found',
  ENTITY_ALREADY_EXISTS: 'entity_already_exists',
};

export const UserCodeError = {
  INVALID_OLD_PASSWORD: 'invalid_old_password',
};
